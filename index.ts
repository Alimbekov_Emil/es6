
const input: HTMLInputElement = document.querySelector('#field')
const btn: HTMLElement = document.querySelector("#btn")
const list: HTMLElement = document.querySelector("#list")

type TodoItem = {
    todo: string,
    checked: boolean,
}

let toDoList: Array<TodoItem> = []

const diplayMessages = (): void => {
    let dispalyMessage: string = "";
    if (toDoList.length === 0) list.innerHTML = "Список Задач пуст"

    toDoList.forEach((item: TodoItem, index: number): void => {
        dispalyMessage += `
        <li>
            <label>
                <input type="checkbox" ${item.checked ? "checked" : ""} />
                <span id="check_${index}" >${item.todo}</span>
                <button class="btn" id="item_${index}">Delete</button>
            </label>
        </li>`
        list.innerHTML = dispalyMessage
    })
}

if (localStorage.getItem("todo")) {
    toDoList = JSON.parse(localStorage.getItem("todo"))
    diplayMessages()
}

btn.addEventListener("click", (): void => {
    if (input.value !== "") {
        const newTodo: TodoItem = {
            todo: input.value,
            checked: false,
        }
        toDoList.push(newTodo)
        diplayMessages()
        localStorage.setItem("todo", JSON.stringify(toDoList))
    }
    input.value = ""
})

list.addEventListener('click', (e: any): void => {
    toDoList.forEach((item: TodoItem, index: number) => {
        if (`item_${index}` === e.target.id) {
            toDoList.splice(index, 1)
        } else if (`check_${index}` === e.target.id) {
            item.checked = !item.checked
        }
        localStorage.setItem("todo", JSON.stringify(toDoList))
        diplayMessages()
    })
})